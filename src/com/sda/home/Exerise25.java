package com.sda.home;

import java.util.Random;

public class Exerise25 {
    /**
     * Zadanie 25:
     * Napisać program, który utworzy tablicę 20 liczb całkowitych z przedziału 1 . . . 10,
     * a następnie wypisze na ekranie ile razy każda z liczb z tego przedziału powtarza
     * się w tablicy.
     * Przykład:
     * Wylosowane liczby: 6 5 4 5 10 5 8 3 10 6 6 6 4 3 2 8 1 3 4 7
     * Wystąpienia:
     * 1 - 1
     * 2 - 1
     * 3 - 3
     * 4 - 3
     * 5 - 3
     * 6 - 4
     * 7 - 1
     * 8 - 2
     * 9 - 0
     * 10 - 2
     */

    public static void main(String[] args) {
        Random r = new Random();
        int[] wygenerowanaTablica = new int[20];
        for (int i = 0; i < wygenerowanaTablica.length; i++) {
            wygenerowanaTablica[i] = r.nextInt(10) + 1;
            System.out.print(wygenerowanaTablica[i] + " ");
        }
        System.out.println();

        int[] wystapienia = new int[11];
        for (int i = 0; i < wygenerowanaTablica.length; i++) {
            wystapienia[wygenerowanaTablica[i]]++;
        }

        for (int i = 0; i < wystapienia.length; i++) {
            System.out.println(i + " -> " + wystapienia[i]);
        }

    }
}
