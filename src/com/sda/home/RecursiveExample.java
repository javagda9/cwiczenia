package com.sda.home;

public class RecursiveExample {
    public static void main(String[] args) {
        int wynik = sumaKolejnychLiczb(5);
        System.out.println(wynik);

        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};

        System.out.println(searchIterative(tablica, 10));
        System.out.println(searchRecursive(tablica, 10, 0, tablica.length-1));
    }

    public static int searchRecursive(int[] tablica, int szukany, int L, int P) {
        if (L == P && szukany == tablica[L]) {
            return L;
        } else {
            int middle = (L + P) / 2;
            if (tablica[middle] > szukany) {
                return searchRecursive(tablica, szukany, L, middle);
            } else if (tablica[middle] < szukany) {
                return searchRecursive(tablica, szukany, middle, P);
            }else  if(tablica[middle] == szukany){
                return middle;
            }
        }
        return -1;
    }

    /**
     * Metoda szuka elementu i zwraca indeks tego elementu
     *
     * @param tablica
     * @param szukany
     * @return
     */
    public static int searchIterative(int[] tablica, int szukany) {
        int L = 0;
        int P = tablica.length - 1;

        System.out.println(tablica.length);
        while (L != P) {
            int middle = (L + P) / 2;
//            int middle = ((P - L) / 2) + L;
            if (tablica[middle] > szukany) {
                P = middle;
            } else if (tablica[middle] < szukany) {
                L = middle;
            } else if (tablica[middle] == szukany) {
                return middle;
            }
        }
        if (L == P && szukany != tablica[L]) {
            // rzucić exception
            // zwrócić wartość charakterystyczna = -1
            throw new ElementNotFoundException();
        }
        if (L == P && szukany == tablica[L]) {
            return L;
        }
        return -1;
    }


    // Fibonacci
    public static int F(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return F(n - 1) + F(n - 2);
        }
    }

    public static int silnia(int n) {
        if (n == 1) {
            return 1;
        } else {
            return n * silnia(n - 1);
        }
    }

    // dla zadanej liczby n
    // metoda zwracała sume kolejnych licz 0 + 1 + 2 + 3 +... n-1 + n
    public static int sumaKolejnychLiczb(int n) {
        if (n == 1) {
            return 1;
//            return n; // to samo
        } else {
            return n + sumaKolejnychLiczb(n - 1);
        }
    }
}
