package com.sda.home.annotations;


public class Adult {
    @MinMaxValue(minValue = 18, maxValue = 100)
    private int age;

    public void setAge(int age) {
        this.age = age;
        if(AdultAgeVerifier.verifyAge(this)){
            System.out.println("Wiek ok");
        }else {
            System.out.println("Wiek nie ok");
        }
    }
}
