package com.sda.home.annotations;

import java.lang.reflect.Field;

public class AdultAgeVerifier {

    public static boolean verifyAge(Adult o){

        Field[] polaKlasy = o.getClass().getDeclaredFields();
        for (Field f :polaKlasy) {
            MinMaxValue annotation = f.getAnnotation(MinMaxValue.class);
            if(annotation != null){ // różna od null jeśli uda się znaleźć
                int min = annotation.minValue();
                int max = annotation.maxValue();

                f.setAccessible(true);
                try {
                    int age = f.getInt(o);
                    if(age > min && age < max){
                        return true;
                    }else {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
}
