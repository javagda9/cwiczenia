package com.sda.home.enumExample;

import java.util.Scanner;

public class MainEnum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj imie:");
        String imie = sc.next();

        System.out.println("Podaj płeć: (MALE/FEMALE)");
        String gender = sc.next().trim().toUpperCase();

        Gender mojaPlec = Gender.valueOf(gender);


        System.out.println(mojaPlec);
        Student student = new Student("Marian", Gender.MALE);
    }
}
