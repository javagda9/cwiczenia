package com.sda.home.enumExample;

public class Student {
    private String name;
    private Gender type;

    public Student(String name, Gender gender) {
        this.name = name;
        this.type = gender;
    }
}
