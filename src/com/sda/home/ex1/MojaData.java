package com.sda.home.ex1;

public class MojaData {
    private static String[] miesiące = new String[] {"sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paz", "lis", "gru"};
    private int dzien;
    private int miesiac;
    private int rok;

    public MojaData(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;
    }

    // c
    public String wypiszDate4(){
        return dzien + " " + miesiące[miesiac-1] + " " + rok;
    }

    // a
    public String wypiszDate() {
        return dzien + "." + miesiac + "." + rok;
    }

    //20.6.1993
    //20.06.1993
    //2.6.1993
    //02.06.1993
    public String wypiszDate2() {
        String dzienString = "";
        String miesiacString = "";
        if (dzien < 10) {
            dzienString = "0" + dzien;
        } else {
            dzienString = "" + dzien;
        }
        if (miesiac < 10) {
            miesiacString = "0" + miesiac;
        } else {
            miesiacString = "" + miesiac;
        }
        return dzienString + "." + miesiacString + "." + rok;
    }

    public String wypiszDate3() {
        String dzienString = "" + ((dzien < 10) ? ("0" + dzien) : (dzien));
        String miesiacString = "" + ((miesiac < 10) ? ("0" + miesiac) : (miesiac));

        return dzienString + "." + miesiacString + "." + rok;

    }

    public int getDzien() {
        return dzien;
    }

    public void setDzien(int dzien) {
        this.dzien = dzien;
    }

    public int getMiesiac() {
        return miesiac;
    }

    public void setMiesiac(int miesiac) {
        this.miesiac = miesiac;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }
    //
//    public String wypisz() {
//        String dzienString = "";
//        if (dzien < 10) {
//            if (dzien < 0) {
//                dzienString = "0" + (dzien * -1);
//            } else {
//                dzienString = "0" + (dzien);
//            }
//        } else {
//            dzienString = "" + dzien;
//        }
//        String dzienString2 = "" + ((dzien < 10) ? (dzien < 0 ? ("0" + (dzien * -1)) : ("0" + dzien)) : (dzien));
//
//    }
}
