package com.sda.home.ex1;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Wydarzenie {
    private String nazwa;
    private MojaData data;

    public Wydarzenie(String nazwa, MojaData data) {
        this.nazwa = nazwa;
        this.data = data;
    }

    public int ilePozostaloLat() {
        return data.getRok() - 2018;
//        return data.getRok() - LocalDateTime.now().getYear();
    }

    public int ilePozostaloMiesiecy() {
        return data.getMiesiac() - 02;
//        return data.getMiesiac() -LocalDateTime.now().getMonthValue();
    }

    public int ilePozostaloDni() {
        return data.getDzien() - 03;
//        return data.getDzien() -LocalDateTime.now().getDayOfMonth();
    }


}
