package com.sda.home.ex2;

import java.util.ArrayList;

public class Biblioteka {
    // dostepneKsiazki - to lista ksiazek w bibliotece
    private ArrayList<Book> dostepneKsiazki = new ArrayList<>();

    // wypozyczone ksiazki
    private ArrayList<Book> wypozyczoneKsiazki = new ArrayList<>();

    // metoda dodania ksiazki do biblioteki
    public void dodajKsiazke(Book ksiazka) {
        // dodajemy ksiazke do listy ksiazek w bibliotece
        dostepneKsiazki.add(ksiazka);
    }

    public boolean czyKsiazkaJestDostepna(String nazwa) {
        // pętla przez wszystkie książki w liście
        for (int i = 0; i < dostepneKsiazki.size(); i++) {

            // jeśli książka na indeksie i ma taką nazwę, jak nazwa szukana (parametr metody)
            if (dostepneKsiazki.get(i).getNazwa().equals(nazwa)) {
                // n
                // zwracamy true - znaleźliśmy książkę
                return true;
            }
        }
        // jeśli wyszliśmy z pętli, to znaczy że książki o podanej nazwie nie znaleźliśmy
        return false;
    }

    public Book wypozyczKsiazke(String nazwa) {
        for (int i = 0; i < dostepneKsiazki.size(); i++) {
            if (dostepneKsiazki.get(i).getNazwa().equals(nazwa)) {
                wypozyczoneKsiazki.add(dostepneKsiazki.get(i));
                Book ksiazkaDoWypozyczenia = dostepneKsiazki.get(i);
                dostepneKsiazki.remove(i);
                return ksiazkaDoWypozyczenia;
                // remove to wybranie i usuniecie elementu na indeksie i
//                    return dostepneKsiazki.remove(i);
            }
        }

        return null;
    }

    public void wyswietlWypozyczoneKsiazki(){
        for (Book ksiazka: wypozyczoneKsiazki) {
            System.out.println(ksiazka);
        }
    }

    public ArrayList<Book> getWypozyczoneKsiazki() {
        return wypozyczoneKsiazki;
    }
}
