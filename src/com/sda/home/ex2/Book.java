package com.sda.home.ex2;

public class Book {
    private String nazwa;
    private String autor;
    private int rokWydania;

    public Book(String nazwa, String autor, int rokWydania) {
        this.nazwa = nazwa;
        this.autor = autor;
        this.rokWydania = rokWydania;
    }

    //„Krzyżacy”, Henryk Sienkiewicz - 2004
    @Override
    public String toString() {
        return " \"" + nazwa + "\", " + autor + " - " + rokWydania;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getRokWydania() {
        return rokWydania;
    }

    public void setRokWydania(int rokWydania) {
        this.rokWydania = rokWydania;
    }
}
