package com.sda.home.ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka();

        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;

        while(isWorking){
            String line = sc.nextLine().trim().toLowerCase();

            String[] slowa = line.split(" ");
            String komenda = slowa[0];

            if(komenda.equals("dodaj")){
                String title = slowa[1];
                String autor = slowa[2];
                int rok = Integer.parseInt(slowa[3]);

                Book nowaKsiazka = new Book(title, autor, rok);

                biblioteka.dodajKsiazke(nowaKsiazka);
            }else if(komenda.equals("wypozycz")){
                String title = slowa[1];

                Book wypozyczona = biblioteka.wypozyczKsiazke(title);
                if(wypozyczona == null){
                    System.out.println("Nie udalo sie wypozyczyc");
                }else {
                    System.out.println("Wypozyczylem ksiazke");
                }
            }else if(komenda.equals("oddaj")){

            }else if(komenda.equals("wypisz_wypozyczone")){
                biblioteka.wyswietlWypozyczoneKsiazki();
            }else if(komenda.equals("wypisz")){

            }else if(komenda.equals("quit")){
                break;
            }
        }
    }
}
