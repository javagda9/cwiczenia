package com.sda.home.ex3;

import java.util.Scanner;

public class MainMovies {
    public static void main(String[] args) {
        MoviesDatabase moviesDatabase = new MoviesDatabase();
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking){
            String line = sc.nextLine().trim().toUpperCase();

            String[] words = line.split(" ");
            String command = words[0];
            if(command.equals("DODAJ")){
                String title = words[1];
                String autor = words[2];
                MovieType type = MovieType.valueOf(words[3]);

                Movie movie = new Movie(title, autor, type);
                moviesDatabase.addMovie(movie);
                System.out.println("Film dodany");
            }else if(command.equals("WYSZUKAJ_TYP")){
                String typ = words[1];
                MovieType type = MovieType.valueOf(typ);

                moviesDatabase.printMoviesOfType(type);
            }else if(command.equals("WYPISZ")){
                moviesDatabase.printAll();
            }else if(command.equals("WYSZUKAJ_NAZWA")){
                String szukanyTytul = words[1];
                Movie znalezionyFilm = moviesDatabase.findMovie(szukanyTytul);
                if(znalezionyFilm == null){
                    System.out.println("Nie znaleziono");
                }else{
                    System.out.println("Znaleziono");
                }
            }else if(command.equals("QUIT")){
                isWorking = false;
                break;
            }
        }
    }
}
