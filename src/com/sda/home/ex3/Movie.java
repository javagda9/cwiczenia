package com.sda.home.ex3;

public class Movie {
    private String name;
    private String author;
    private MovieType type;

    public Movie(String name, String author, MovieType type) {
        this.name = name;
        this.author = author;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MovieType getType() {
        return type;
    }

    public void setType(MovieType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", type=" + type +
                '}';
    }
}
