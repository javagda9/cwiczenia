package com.sda.home.ex3;

import java.util.ArrayList;

public class MoviesDatabase {
    private ArrayList<Movie> movies = new ArrayList<>();

    public void addMovie(Movie doDodania) {
        movies.add(doDodania);
    }

    public Movie findMovie2(String name) {
        for (int i = 0; i < movies.size(); i++) {
            Movie movie = movies.get(i);
            if (movie.getName().equals(name)) {
                return movie;
            }
        }
        return null;
    }

    public Movie findMovie(String name) {
        for (Movie movie : movies) {
            if (movie.getName().equals(name)) {
                return movie;
            }
        }
        return null;
    }

    public void printMoviesOfType(MovieType type){
        for (Movie movie : movies) {
            if (movie.getType()== type) {
                System.out.println(movie);
            }
        }
    }

    public ArrayList<Movie> getMoviesOfType(MovieType type){
        ArrayList<Movie> nowaLista = new ArrayList<>();
        for (Movie movie : movies) {
            if (movie.getType()== type) {
                nowaLista.add(movie);
            }
        }
        return nowaLista;
    }

    public void printAll() {

    }
}
