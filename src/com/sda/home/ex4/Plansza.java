package com.sda.home.ex4;

public class Plansza {
    public static double obliczDystans(Punkt a, Punkt b) {

        // wart. bezwzgl. z roznicy x
        double roznicaX = Math.abs(a.getX() - b.getX());
        // wart. bezwzgl. z roznicy y
        double roznicaY = Math.abs(a.getY() - b.getY());

        // pierw. z sumy kwadratow roznicy x i y
        return Math.sqrt(roznicaX * roznicaX + roznicaY * roznicaY);
    }
}
